﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;

namespace SemanticVersion {
	public class SemanticVersion : IComparable, IComparable<SemanticVersion>, IEquatable<SemanticVersion> {
		// base regex from https://semver.org/spec/v2.0.0.html
		private static readonly Regex _parseRegex = new Regex(@"^(0|[1-9]\d*)(?:\.(0|[1-9]\d*))?(?:\.(0|[1-9]\d*))?(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$", RegexOptions.Compiled);

		private const string _alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		private const string _numbers = "0123456789";
		private const string _punctuation = ".";
		private static readonly IReadOnlyCollection<char> _legalPreReleaseCharacters = (_alphabet + _numbers + _punctuation).ToCharArray();
		private static readonly IReadOnlyCollection<char> _legalMetaDataCharacters = _legalPreReleaseCharacters;

		public int Major { get; private set; }
		public int Minor { get; private set; }
		public int Patch { get; private set; }
		private IReadOnlyCollection<PreReleaseIdentifier> _preReleaseIdentifiers;
		private string _preReleaseString;
		public string PreRelease {
			get => _preReleaseString;
			private set {
				if (string.IsNullOrEmpty(value)) {
					_preReleaseString = string.Empty;
					_preReleaseIdentifiers = new PreReleaseIdentifier[0];
				} else {
					if (value.Any(c => !_legalPreReleaseCharacters.Contains(c))) {
						throw new ArgumentException("Illegal character in PreRelease string.");
					}
					_preReleaseIdentifiers = value.Split('.').Select(i => new PreReleaseIdentifier(i)).ToList();
					_preReleaseString = value;
				}
			}
		}
		private string _buildMetadataString;
		public string BuildMetadata {
			get => _buildMetadataString;
			private set {
				if (string.IsNullOrEmpty(value)) {
					_buildMetadataString = string.Empty;
				} else {
					if (value.Any(c => !_legalMetaDataCharacters.Contains(c))) {
						throw new ArgumentException("Illegal character in Build Metadata string.");
					}
					_buildMetadataString = value;
				}
			}
		}
		private string _versionString;

		/// <summary>
		/// Create a Semantic Version; see https://semver.org/spec/v2.0.0.html
		/// </summary>
		/// <param name="v">exact semantic version: 'major.minor.patch-prerelease+metadata'; major, minor, and patch are required</param>
		/// <param name="allowShortVersions">when true, allows for missing patch or minor+patch values (sets them to 0)</param>
		public SemanticVersion(string v, bool allowShortVersions = false) {
			bool recomputeVersionString = false;
			try {
				var matches = _parseRegex.Matches(v)[0].Groups;
				Major = getVersionPortion(matches, 1, true);
				Minor = getVersionPortion(matches, 2, !allowShortVersions);
				Patch = getVersionPortion(matches, 3, !allowShortVersions);
				PreRelease = matches[4].Value ?? string.Empty;
				BuildMetadata = matches[5].Value ?? string.Empty;

				_versionString = recomputeVersionString
					? ReCalculateVersionString()
					: v;
			} catch (Exception ex) {
				throw new ArgumentException(v is null
					? "Could not parse a null string to a version."
					: $"Could not parse <<{v}>> to a version."
					, ex);
			}

			int getVersionPortion(GroupCollection matches, int matchNumber, bool shouldThrow) {
				string input = shouldThrow
					? matches[matchNumber].Value
					: matches[matchNumber]?.Value;
				if (int.TryParse(input, out int result)) {
					return result;
				}
				if (shouldThrow) {
					// throw the appropriate exception; double-parsing is generally preferable to flow control via exceptions
					int.Parse(input);
				}
				recomputeVersionString = true;
				return 0;
			}
		}

		public SemanticVersion(int major, int minor, int patch, string preRelease = null, string metadata = null) {
			Major = major >= 0 ? major : throw new ArgumentException("Major version cannot be negative.");
			Minor = minor >= 0 ? minor : throw new ArgumentException("Minor version cannot be negative.");
			Patch = patch >= 0 ? patch : throw new ArgumentException("Patch version cannot be negative.");
			PreRelease = preRelease;
			BuildMetadata = metadata;
			_versionString = ReCalculateVersionString();
		}

		public int CompareTo([AllowNull] SemanticVersion other) {
			if (other is null) {
				throw new ArgumentNullException();
			}
			int compareResult = Major.CompareTo(other.Major);
			if (compareResult != 0) {
				return compareResult;
			}
			compareResult = Minor.CompareTo(other.Minor);
			if (compareResult != 0) {
				return compareResult;
			}
			compareResult = Patch.CompareTo(other.Patch);
			if (compareResult != 0) {
				return compareResult;
			}

			var thisIdentifierEnumerator = _preReleaseIdentifiers.GetEnumerator();
			var otherIdentifierEnumerator = other._preReleaseIdentifiers.GetEnumerator();

			while (thisIdentifierEnumerator.MoveNext() && otherIdentifierEnumerator.MoveNext()) {
				compareResult = thisIdentifierEnumerator.Current.CompareTo(otherIdentifierEnumerator.Current);
				if (compareResult != 0) {
					return compareResult;
				}
			}

			return _preReleaseIdentifiers.Count.CompareTo(other._preReleaseIdentifiers.Count);
		}

		public int CompareTo(object obj) => CompareTo(obj as SemanticVersion);

		public bool Equals([AllowNull] SemanticVersion other) {
			return other is object
				&& Major == other.Major
				&& Minor == other.Minor
				&& Patch == other.Patch
				&& string.Equals(PreRelease, other.PreRelease)
				&& string.Equals(BuildMetadata, other.BuildMetadata);
		}

		public override bool Equals(object obj) => Equals(obj as SemanticVersion);

		public override int GetHashCode() => HashCode.Combine(Major, Minor, Patch, PreRelease, BuildMetadata);

		public override string ToString() => _versionString;

		private string ReCalculateVersionString() {
			return $"{Major}.{Minor}.{Patch}{(string.IsNullOrWhiteSpace(PreRelease) ? string.Empty : $"-{PreRelease}")}{(string.IsNullOrWhiteSpace(BuildMetadata) ? string.Empty : $"+{BuildMetadata}")}";
		}

		public static bool operator <(SemanticVersion a, SemanticVersion b) {
			return a is object && b is object && (a.CompareTo(b) < 0);
		}

		public static bool operator >(SemanticVersion a, SemanticVersion b) {
			return a is object && b is object && (a.CompareTo(b) > 0);
		}

		public static bool operator ==(SemanticVersion a, SemanticVersion b) {
			return ReferenceEquals(a, b) || a is object && b is object && (a.CompareTo(b) == 0);
		}

		public static bool operator !=(SemanticVersion a, SemanticVersion b) {
			return !(a == b);
		}

		public static bool operator >=(SemanticVersion a, SemanticVersion b) {
			return (a > b) || (a == b);
		}

		public static bool operator <=(SemanticVersion a, SemanticVersion b) {
			return (a < b) || (a == b);
		}

		private class PreReleaseIdentifier : IComparable<PreReleaseIdentifier> {
			public uint? IntValue { get; }
			public string StringValue { get; }

			public PreReleaseIdentifier(string chunk) {
				if (chunk is null) {
					throw new ArgumentNullException();
				}
				if (uint.TryParse(chunk, out uint value)) {
					IntValue = value;
				} else {
					StringValue = chunk;
				}
			}

			public int CompareTo([AllowNull] PreReleaseIdentifier other) {
				if (other is null) {
					throw new ArgumentNullException();
				}

				if (IntValue.HasValue && other.IntValue.HasValue) {
					return IntValue.Value.CompareTo(other.IntValue.Value);
				}
				if (IntValue.HasValue) {
					return -1;
				}
				if (other.IntValue.HasValue) {
					return 1;
				}
				return StringValue.CompareTo(other.StringValue);
			}
		}
	}
}
