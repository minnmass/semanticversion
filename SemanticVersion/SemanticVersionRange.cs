﻿using System;
using System.Linq;

namespace SemanticVersion {
	public class SemanticVersionRange {
		private static readonly SemanticVersion _zeroVersion = new SemanticVersion(0, 0, 0);
		private static readonly SemanticVersion _intMaxVersion = new SemanticVersion(int.MaxValue, int.MaxValue, int.MaxValue);

		public SemanticVersion Start { get; }
		public SemanticVersion End { get; }
		public bool IncludeStart { get; }
		public bool IncludeEnd { get; }

		public SemanticVersionRange(SemanticVersion start, SemanticVersion end, bool includeStart, bool includeEnd) {
			if (end < start) {
				throw new ArgumentException($"Error: {nameof(start)} (\"{start}\") cannot be a higher version than {nameof(end)} (\"{end}\".)");
			}
			Start = start;
			End = end;
			IncludeStart = includeStart;
			IncludeEnd = includeEnd;
		}

		private static readonly char[] _inclusionChars = "[()]".ToCharArray();
		public SemanticVersionRange(string input) {
			var trimmed = input.Trim(_inclusionChars);
			var stringLengthDifference = input.Length - trimmed.Length;
			if (stringLengthDifference == 0) {
				if (input.Count(c => c == '*') == 1) {
					var indexAfterWildcard = input.IndexOf('*') + 1;
					if (indexAfterWildcard == input.Length || input[indexAfterWildcard] == '-' || input[indexAfterWildcard] == '+') {

						var flagInt = Enumerable.Range(0, int.MaxValue).Select(i => int.MaxValue - i).First(i => !input.Contains(i.ToString()));
						var tmpVersion = new SemanticVersion(input.Replace("*", flagInt.ToString()), allowShortVersions: true);

						if (tmpVersion.Major == flagInt) {
							Start = _zeroVersion;
							IncludeStart = true;
							End = _intMaxVersion;
							IncludeEnd = true;
						} else if (tmpVersion.Minor == flagInt) {
							Start = new SemanticVersion(tmpVersion.Major, 0, 0);
							IncludeStart = true;
							End = new SemanticVersion(tmpVersion.Major + 1, 0, 0);
							IncludeEnd = false;
						} else { // wildcard minor
							Start = new SemanticVersion(tmpVersion.Major, tmpVersion.Minor, 0);
							IncludeStart = true;
							End = new SemanticVersion(tmpVersion.Major, tmpVersion.Minor + 1, 0);
							IncludeEnd = false;
						}
					} else {
						throw new ArgumentException("Illegal input string: wildcard must terminate the string or be followed by a '-' or '+'.");
					}
				}

				try {
					Start = new SemanticVersion(input, allowShortVersions: true);
				} catch (Exception ex) {
					throw new ArgumentException("Illegal input string: could not parse to single version.", ex);
				}
				End = _intMaxVersion;
				IncludeStart = true;
				IncludeEnd = true;
				return;
			}
			if (stringLengthDifference != 2) {
				throw new ArgumentException("Illegal input string: bad bracketing.");
			}

			switch (input[0]) {
				case '[':
					IncludeStart = true;
					break;
				case '(':
					IncludeStart = false;
					break;
				default:
					throw new ArgumentException("Illegal input string: does not start with '[' or '(' and is not a single version.");
			}
			switch (input[input.Length - 1]) {
				case ']':
					IncludeEnd = true;
					break;
				case ')':
					IncludeEnd = false;
					break;
				default:
					throw new ArgumentException("Illegal input string: does not end with ']' or ')' and is not a single version.");
			}

			var versions = trimmed.Split(',');
			if (versions.Length > 2) {
				throw new ArgumentException("Illegal input string: too many commas.");
			}
			if (versions.Length == 1) {
				if (IncludeStart && IncludeEnd) {
					Start = new SemanticVersion(trimmed, allowShortVersions: true);
					End = Start;
				} else {
					throw new ArgumentException("Illegal input string: strings with only one version must be bracketed by '[' and ']' or must not be bracketed at all (eg., \"1.0\" or \"[1.0]\").");
				}
			} else {
				if (string.IsNullOrEmpty(versions[0])) {
					Start = _zeroVersion;
					IncludeStart = true;
				} else {
					Start = new SemanticVersion(versions[0], allowShortVersions: true);
				}
				if (string.IsNullOrEmpty(versions[1])) {
					End = _intMaxVersion;
					IncludeEnd = true;
				} else {
					End = new SemanticVersion(versions[1], allowShortVersions: true);
				}
			}

			if (End < Start) {
				throw new ArgumentException($"Error: start (\"{Start}\") cannot be a higher version than end (\"{End}\".)");
			}
		}

		public bool Includes(SemanticVersion version) {
			bool lowerBoundOkay = IncludeStart
				? version >= Start
				: version > Start;

			bool upperBoundOkay = IncludeEnd
				? version <= End
				: version < End;

			return lowerBoundOkay && upperBoundOkay;
		}
	}
}
