Semantic Version utilities, referencing https://semver.org/spec/v2.0.0.html

When complete, will be able to:

* parse semantic versions
* compare version 
* determine whether a version is within a version range/wildcard per NuGet's extensions: https://docs.microsoft.com/en-us/nuget/concepts/package-versioning#semantic-versioning-200