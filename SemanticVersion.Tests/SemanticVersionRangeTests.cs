﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SemanticVersionRangeTests {
	public class SemanticVersionRangeTests {
		[Fact]
		public void NonParsingConstructor_LegalValues_StoresDataCorrectly() {
			var lower = new SemanticVersion.SemanticVersion(1, 0, 0);
			var upper = new SemanticVersion.SemanticVersion(2, 0, 0);
			var includeLower = false;
			var includeUpper = true;

			var range = new SemanticVersion.SemanticVersionRange(lower, upper, includeLower, includeUpper);

			Assert.Same(lower, range.Start);
			Assert.Same(upper, range.End);
			Assert.Equal(includeLower, range.IncludeStart);
			Assert.Equal(includeUpper, range.IncludeEnd);
		}

		[Fact]
		public void NonParsingConstructor_LowerVersionGreaterThanGreaterVersion_Throws() {
			var lower = new SemanticVersion.SemanticVersion(2, 0, 0);
			var upper = new SemanticVersion.SemanticVersion(1, 0, 0);

			var exception = Assert.Throws<ArgumentException>(() => new SemanticVersion.SemanticVersionRange(lower, upper, true, true));
		}

		[Theory, ClassData(typeof(ValidInputStringData))]
		public void Constructor_ParsesValidInputStringsCorrectly(RangeTestDatum datum) {
			var range = new SemanticVersion.SemanticVersionRange(datum.GenerationString);

			Assert.Equal(datum.LowerBound, range.Start);
			Assert.Equal(datum.UpperBound, range.End);
			Assert.Equal($"lower included: {datum.LowerBoundIncluded}", $"lower included: {range.IncludeStart}");
			Assert.Equal($"upper included: {datum.UpperBoundIncluded}", $"upper included: {range.IncludeEnd}");
		}

		[Theory]
		[InlineData("")]
		[InlineData("(1.0)")]
		[InlineData("(2.0,1.0)")]
		[InlineData("lkashgdlfkashgldf")]
		[InlineData("(1.0")]
		[InlineData("1.0)")]
		[InlineData("(1.0,2.0,3.0)")]
		[InlineData("(1.*)")]
		[InlineData("(1.0,1.*)")]
		[InlineData("1.*.*")]
		public void Constructor_IllegalInputs_ThrowsException(string input) {
			var exception = Record.Exception(() => new SemanticVersion.SemanticVersionRange(input));

			Assert.NotNull(exception);
		}

		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		public void Includes_LowerBoundIncluded_Correct(bool included) {
			var lower = new SemanticVersion.SemanticVersion(1, 0, 0);
			var upper = new SemanticVersion.SemanticVersion(2, 0, 0);
			var range = new SemanticVersion.SemanticVersionRange(lower, upper, included, true);

			var actual = range.Includes(lower);

			Assert.Equal(included, actual);
		}

		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		public void Includes_UpperBoundIncluded_Correct(bool included) {
			var lower = new SemanticVersion.SemanticVersion(1, 0, 0);
			var upper = new SemanticVersion.SemanticVersion(2, 0, 0);
			var range = new SemanticVersion.SemanticVersionRange(lower, upper, true, included);

			var actual = range.Includes(upper);

			Assert.Equal(included, actual);
		}

		[Fact]
		public void Includes_IntermediateValue_ReturnsTrue() {
			var lower = new SemanticVersion.SemanticVersion(1, 0, 0);
			var value = new SemanticVersion.SemanticVersion(2, 0, 0);
			var upper = new SemanticVersion.SemanticVersion(3, 0, 0);
			var range = new SemanticVersion.SemanticVersionRange(lower, upper, false, false);

			var actual = range.Includes(value);

			Assert.True(actual);
		}

		[Fact]
		public void Includes_LowerValue_ReturnsTrue() {
			var value = new SemanticVersion.SemanticVersion(0, 0, 0);
			var lower = new SemanticVersion.SemanticVersion(1, 0, 0);
			var upper = new SemanticVersion.SemanticVersion(3, 0, 0);
			var range = new SemanticVersion.SemanticVersionRange(lower, upper, false, false);

			var actual = range.Includes(value);

			Assert.False(actual);
		}

		[Fact]
		public void Includes_HigherValue_ReturnsTrue() {
			var lower = new SemanticVersion.SemanticVersion(1, 0, 0);
			var upper = new SemanticVersion.SemanticVersion(3, 0, 0);
			var value = new SemanticVersion.SemanticVersion(5, 0, 0);
			var range = new SemanticVersion.SemanticVersionRange(lower, upper, false, false);

			var actual = range.Includes(value);

			Assert.False(actual);
		}
	}

	public class RangeTestDatum {
		public string GenerationString { get; set; }
		public SemanticVersion.SemanticVersion LowerBound { get; set; }
		public SemanticVersion.SemanticVersion UpperBound { get; set; }
		public bool LowerBoundIncluded { get; set; }
		public bool UpperBoundIncluded { get; set; }
	}

	public class ValidInputStringData : IEnumerable<object[]> {
		static ValidInputStringData() {
			// source: https://docs.microsoft.com/en-us/nuget/concepts/package-versioning#semantic-versioning-200
			var data = new[] {
				new RangeTestDatum {
					GenerationString = "1.0",
					LowerBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					LowerBoundIncluded = true,
					UpperBound = new SemanticVersion.SemanticVersion(int.MaxValue, int.MaxValue, int.MaxValue),
					UpperBoundIncluded = true
				},
				new RangeTestDatum {
					GenerationString = "(1.0,)",
					LowerBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					LowerBoundIncluded = false,
					UpperBound = new SemanticVersion.SemanticVersion(int.MaxValue, int.MaxValue, int.MaxValue),
					UpperBoundIncluded = true
				},
				new RangeTestDatum {
					GenerationString = "[1.0]",
					LowerBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					LowerBoundIncluded = true,
					UpperBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					UpperBoundIncluded = true
				},
				new RangeTestDatum {
					GenerationString = "(,1.0]",
					LowerBound = new SemanticVersion.SemanticVersion(0, 0, 0),
					LowerBoundIncluded = true,
					UpperBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					UpperBoundIncluded = true
				},
				new RangeTestDatum {
					GenerationString = "(,1.0)",
					LowerBound = new SemanticVersion.SemanticVersion(0, 0, 0),
					LowerBoundIncluded = true,
					UpperBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					UpperBoundIncluded = false
				},
				new RangeTestDatum{
					GenerationString = "[1.0,2.0]",
					LowerBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					LowerBoundIncluded = true,
					UpperBound = new SemanticVersion.SemanticVersion(2, 0, 0),
					UpperBoundIncluded = true
				},
				new RangeTestDatum {
					GenerationString = "(1.0,2.0)",
					LowerBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					LowerBoundIncluded = false,
					UpperBound = new SemanticVersion.SemanticVersion(2, 0, 0),
					UpperBoundIncluded = false
				},
				new RangeTestDatum {
					GenerationString = "[1.0,2.0)",
					LowerBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					LowerBoundIncluded = true,
					UpperBound = new SemanticVersion.SemanticVersion(2, 0, 0),
					UpperBoundIncluded = false
				},
				new RangeTestDatum {
					GenerationString = "(1.0,2.0]",
					LowerBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					LowerBoundIncluded = false,
					UpperBound = new SemanticVersion.SemanticVersion(2, 0, 0),
					UpperBoundIncluded = true
				},
				new RangeTestDatum {
					GenerationString = "1.*",
					LowerBound = new SemanticVersion.SemanticVersion (1, 0, 0),
					LowerBoundIncluded = true,
					UpperBound = new SemanticVersion.SemanticVersion(2, 0, 0),
					UpperBoundIncluded = false
				},
				new RangeTestDatum {
					GenerationString = "1.0.*",
					LowerBound = new SemanticVersion.SemanticVersion(1, 0, 0),
					LowerBoundIncluded = true,
					UpperBound = new SemanticVersion.SemanticVersion(1, 1, 0),
					UpperBoundIncluded = false
				}
		};

			_data = data.Select(d => new object[] { d }).ToList();
		}

		private static readonly IReadOnlyCollection<object[]> _data;
		public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}
