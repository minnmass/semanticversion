using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SemanticVersionTests {
	public class SemanticVersionTests {
		[Theory, ClassData(typeof(GoodVersions))]
		public void Constructor_ValidVersionString_ParsesCorrectly(SimpleVersion version) {
			var semanticVersion = new SemanticVersion.SemanticVersion(version.GeneratingString);

			Assert.Equal(version.Major, semanticVersion.Major);
			Assert.Equal(version.Minor, semanticVersion.Minor);
			Assert.Equal(version.Patch, semanticVersion.Patch);
			Assert.Equal(version.PreRelease ?? string.Empty, semanticVersion.PreRelease);
			Assert.Equal(version.BuildMetadata ?? string.Empty, semanticVersion.BuildMetadata);
		}

		[Theory, ClassData(typeof(GoodVersions))]
		public void NonParsingConstructor_GoodVersions_Correct(SimpleVersion version) {
			var semanticVersion = new SemanticVersion.SemanticVersion(version.Major, version.Minor, version.Patch, version.PreRelease, version.BuildMetadata);

			Assert.Equal(version.Major, semanticVersion.Major);
			Assert.Equal(version.Minor, semanticVersion.Minor);
			Assert.Equal(version.Patch, semanticVersion.Patch);
			Assert.Equal(version.PreRelease ?? string.Empty, semanticVersion.PreRelease);
			Assert.Equal(version.BuildMetadata ?? string.Empty, semanticVersion.BuildMetadata);
			Assert.Equal(version.GeneratingString, semanticVersion.ToString());
		}

		[Theory, ClassData(typeof(BadVersions))]
		public void Constructor_InvalidVersionString_ThrowsException(SimpleVersion version) {
			var exception = Assert.Throws<ArgumentException>(() => new SemanticVersion.SemanticVersion(version.GeneratingString));

			Assert.NotNull(exception);
			Assert.Contains(version.GeneratingString ?? "a null string", exception.Message);
		}

		[Theory, ClassData(typeof(BadPrereleaseStrings))]
		public void NonParsingConstructor_InvalidPreRelease_Throws(string prerelease) {
			var exception = Assert.Throws<ArgumentException>(() => new SemanticVersion.SemanticVersion(1, 1, 1, prerelease, string.Empty));

			Assert.NotNull(exception);
		}

		[Theory, ClassData(typeof(BadMetadataStrings))]
		public void NonParsingConstructor_InvalidMetadata_Throws(string metadata) {
			var exception = Assert.Throws<ArgumentException>(() => new SemanticVersion.SemanticVersion(1, 1, 1, string.Empty, metadata));

			Assert.NotNull(exception);
		}

		[Fact]
		public void NonParsingConstructor_NegativeMajor_Throws() {
			var exception = Assert.Throws<ArgumentException>(() => new SemanticVersion.SemanticVersion(-1, 1, 1, string.Empty, string.Empty));

			Assert.NotNull(exception);
			Assert.Equal("Major version cannot be negative.", exception.Message);
		}

		[Fact]
		public void NonParsingConstructor_NegativeMinor_Throws() {
			var exception = Assert.Throws<ArgumentException>(() => new SemanticVersion.SemanticVersion(1, -1, 1, string.Empty, string.Empty));

			Assert.NotNull(exception);
			Assert.Equal("Minor version cannot be negative.", exception.Message);
		}

		[Fact]
		public void NonParsingConstructor_NegativePatch_Throws() {
			var exception = Assert.Throws<ArgumentException>(() => new SemanticVersion.SemanticVersion(1, 1, -1, string.Empty, string.Empty));

			Assert.NotNull(exception);
			Assert.Equal("Patch version cannot be negative.", exception.Message);
		}

		[Fact]
		public void NonParsingConstructor_ZeroValueNumerics_DoesNotThrow() {
			var exception = Record.Exception(() => new SemanticVersion.SemanticVersion(0, 0, 0));

			Assert.Null(exception);
		}

		[Theory]
		[InlineData("1.0", "1.0.0")]
		[InlineData("1", "1.0.0")]
		[InlineData("1+metadata", "1.0.0+metadata")]
		[InlineData("1-prerelease", "1.0.0-prerelease")]
		[InlineData("1.0-prerelease+metadata", "1.0.0-prerelease+metadata")]
		public void Constructor_AllowPartialTrue_MissingPieces_GeneratesExpectedVersions(string input, string expected) {
			var actual = new SemanticVersion.SemanticVersion(input, true).ToString();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("")]
		[InlineData("1..0")]
		[InlineData("1.0.0.0")]
		public void Constructor_AllowPartialTrue_ThrowsWhenExpected(string input) {
			var exception = Record.Exception(() => new SemanticVersion.SemanticVersion(input, true));

			Assert.NotNull(exception);
		}

		[Theory, ClassData(typeof(OrderedVersionStringPairs))]
		public void CompareTo_ReturnsCorrectlySigns(string largerVersionString, string smallerVersionString) {
			var largerVersion = new SemanticVersion.SemanticVersion(largerVersionString);
			var smallerVersion = new SemanticVersion.SemanticVersion(smallerVersionString);

			Assert.True(largerVersion.CompareTo(smallerVersion) > 0);
			Assert.True(smallerVersion.CompareTo(largerVersion) < 0);
		}

		[Fact]
		public void CompareTo_StaticRealityCheck() {
			var largeVersion = new SemanticVersion.SemanticVersion("4.0.0");
			var smallVersion = new SemanticVersion.SemanticVersion("1.0.0");

			Assert.True(largeVersion.CompareTo(smallVersion) > 0);
			Assert.True(smallVersion.CompareTo(largeVersion) < 0);
			Assert.True(smallVersion.CompareTo(smallVersion) == 0);
			Assert.True(largeVersion.CompareTo(largeVersion) == 0);
		}

		[Theory, ClassData(typeof(GoodVersions))]
		public void CompareTo_SameVersions_ReturnsZero(SimpleVersion version) {
			var versionA = new SemanticVersion.SemanticVersion(version.GeneratingString);
			var versionB = new SemanticVersion.SemanticVersion(version.GeneratingString);

			Assert.Equal(0, versionA.CompareTo(versionB));
			Assert.Equal(0, versionB.CompareTo(versionA));
		}

		[Fact]
		public void CompareTo_OnlyDiffersByBuildMetadata_ReturnsZero() {
			var versionA = new SemanticVersion.SemanticVersion("1.0.0-stuff+metadata");
			var versionB = new SemanticVersion.SemanticVersion("1.0.0-stuff+otherMetadata");

			Assert.Equal(0, versionA.CompareTo(versionB));
			Assert.Equal(0, versionB.CompareTo(versionA));
		}

		[Theory, ClassData(typeof(GoodVersions))]
		public void Equals_SameString_Equal(SimpleVersion version) {
			var versionA = new SemanticVersion.SemanticVersion(version.GeneratingString);
			var versionB = new SemanticVersion.SemanticVersion(version.GeneratingString);

			Assert.True(versionA.Equals(versionB));
			Assert.True(versionB.Equals(versionA));
			Assert.Equal(versionA, versionB);
		}

		[Theory, ClassData(typeof(GoodVersions))]
		public void GetHashCode_SameString_Equal(SimpleVersion version) {
			var versionA = new SemanticVersion.SemanticVersion(version.GeneratingString);
			var versionB = new SemanticVersion.SemanticVersion(version.GeneratingString);

			Assert.Equal(versionA.GetHashCode(), versionB.GetHashCode());
		}

		[Theory, ClassData(typeof(OrderedVersionStringPairs))]
		public void GetHashCode_DifferentString_DifferentHashCodes(string stringA, string stringB) {
			var versionA = new SemanticVersion.SemanticVersion(stringA);
			var versionB = new SemanticVersion.SemanticVersion(stringB);

			Assert.NotEqual(versionA.GetHashCode(), versionB.GetHashCode());
		}

		[Theory, ClassData(typeof(GoodVersions))]
		public void ToString_MatchesBuildString(SimpleVersion version) {
			var fullVersion = new SemanticVersion.SemanticVersion(version.GeneratingString);

			Assert.Equal(version.GeneratingString, fullVersion.ToString());
		}

		[InlineData(null, null, true)]
		[InlineData(null, "1.0.0", false)]
		[InlineData("1.0.0", null, false)]
		[InlineData("1.0.0", "1.0.0", true)]
		[InlineData("1.0.0", "2.0.0", false)]
		[InlineData("2.0.0", "1.0.0", false)]
		[Theory]
		public void EqualsOverride_ReturnsAsExpected(string a, string b, bool expected) {
			var versionA = a == null ? null : new SemanticVersion.SemanticVersion(a);
			var versionB = b == null ? null : new SemanticVersion.SemanticVersion(b);

			Assert.Equal(expected, versionA == versionB);
		}

		[InlineData(null, null, false)]
		[InlineData(null, "1.0.0", true)]
		[InlineData("1.0.0", null, true)]
		[InlineData("1.0.0", "1.0.0", false)]
		[InlineData("1.0.0", "2.0.0", true)]
		[InlineData("2.0.0", "1.0.0", true)]
		[Theory]
		public void NotEqualsOverride_ReturnsAsExpected(string a, string b, bool expected) {
			var versionA = a == null ? null : new SemanticVersion.SemanticVersion(a);
			var versionB = b == null ? null : new SemanticVersion.SemanticVersion(b);

			Assert.Equal(expected, versionA != versionB);
		}

		[InlineData(null, null, false)]
		[InlineData(null, "1.0.0", false)]
		[InlineData("1.0.0", null, false)]
		[InlineData("1.0.0", "1.0.0", false)]
		[InlineData("1.0.0", "2.0.0", false)]
		[InlineData("2.0.0", "1.0.0", true)]
		[Theory]
		public void GreaterThanOverride_ReturnsAsExpected(string a, string b, bool expected) {
			var versionA = a == null ? null : new SemanticVersion.SemanticVersion(a);
			var versionB = b == null ? null : new SemanticVersion.SemanticVersion(b);

			Assert.Equal(expected, versionA > versionB);
		}

		[InlineData(null, null, false)]
		[InlineData(null, "1.0.0", false)]
		[InlineData("1.0.0", null, false)]
		[InlineData("1.0.0", "1.0.0", false)]
		[InlineData("1.0.0", "2.0.0", false)]
		[InlineData("2.0.0", "1.0.0", true)]
		[Theory]
		public void LessThanOverride_ReturnsAsExpected(string a, string b, bool expected) {
			var versionA = a == null ? null : new SemanticVersion.SemanticVersion(a);
			var versionB = b == null ? null : new SemanticVersion.SemanticVersion(b);

			Assert.Equal(expected, versionA > versionB);
		}

		[InlineData(null, null, true)]
		[InlineData(null, "1.0.0", false)]
		[InlineData("1.0.0", null, false)]
		[InlineData("1.0.0", "1.0.0", true)]
		[InlineData("1.0.0", "2.0.0", false)]
		[InlineData("2.0.0", "1.0.0", true)]
		[Theory]
		public void GreaterThanOrEqualOverride_ReturnsAsExpected(string a, string b, bool expected) {
			var versionA = a == null ? null : new SemanticVersion.SemanticVersion(a);
			var versionB = b == null ? null : new SemanticVersion.SemanticVersion(b);

			Assert.Equal(expected, versionA >= versionB);
		}

		[InlineData(null, null, true)]
		[InlineData(null, "1.0.0", false)]
		[InlineData("1.0.0", null, false)]
		[InlineData("1.0.0", "1.0.0", true)]
		[InlineData("1.0.0", "2.0.0", true)]
		[InlineData("2.0.0", "1.0.0", false)]
		[Theory]
		public void LessThanOrEqualOverride_ReturnsAsExpected(string a, string b, bool expected) {
			var versionA = a == null ? null : new SemanticVersion.SemanticVersion(a);
			var versionB = b == null ? null : new SemanticVersion.SemanticVersion(b);

			Assert.Equal(expected, versionA <= versionB);
		}

		[Fact]
		public void GetHashCode_DifferentBuildMetadata_DifferentHashCodes() {
			var versionA = new SemanticVersion.SemanticVersion("1.0.0-stuff+metadata");
			var versionB = new SemanticVersion.SemanticVersion("1.0.0-stuff+otherMetadata");

			Assert.NotEqual(versionA.GetHashCode(), versionB.GetHashCode());
		}
	}

	public class OrderedVersionStringPairs : IEnumerable<object[]> {
		static OrderedVersionStringPairs() {
			// 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-alpha.beta < 1.0.0-beta < 1.0.0-beta.2 < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0
			var sortedVersions = new[] {
				"4.0.0",
				"3.9.9",
				"3.8.9",
				"3.8.8",
				"3.8.7-rc.1",
				"3.8.7-beta.11",
				"3.8.7-beta.2",
				"3.8.7-beta",
				"3.8.7-alpha.beta",
				"3.8.7-alpha.1",
				"3.8.7-alpha",
				"3.8.7",
				"1.0.0",
				"0.1.1",
				"0.1.0",
				"0.0.0"
			};

			var versions = new List<object[]>();
			for (int bigIdx = 0; bigIdx < sortedVersions.Length; ++bigIdx) {
				for (int smallIdx = bigIdx + 1; smallIdx < sortedVersions.Length; ++smallIdx) {
					versions.Add(new object[] { sortedVersions[bigIdx], sortedVersions[smallIdx] });
				}
			}
			_versions = versions;
		}

		private static readonly IReadOnlyCollection<object[]> _versions;
		public IEnumerator<object[]> GetEnumerator() => _versions.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}

	public class GoodVersions : IEnumerable<object[]> {
		static GoodVersions() {
			var prereleases = new[] { string.Empty, "-prerelease", "-pre.release" };
			var metadatas = new[] { string.Empty, "+metadata", "+meta.data" };

			var versions = new List<SimpleVersion>(prereleases.Length * metadatas.Length);

			foreach (var prerelease in prereleases) {
				foreach (var metadata in metadatas) {
					versions.Add(new SimpleVersion {
						Major = 1,
						Minor = 2,
						Patch = 3,
						PreRelease = prerelease.TrimStart('-'),
						BuildMetadata = metadata.TrimStart('+'),
						GeneratingString = $"1.2.3{prerelease}{metadata}"
					});
				}
			}

			_versions = versions.Select(v => new object[] { v }).ToList();
		}

		private static readonly IReadOnlyCollection<object[]> _versions;
		public IEnumerator<object[]> GetEnumerator() => _versions.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}

	public class BadPrereleaseStrings : IEnumerable<object[]> {
		static BadPrereleaseStrings() {
			var prereleases = new[] { "-", "pre-release" };

			_versions = prereleases.Select(i => new object[] { i }).ToList();
		}

		public static int Length => _versions.Count;

		private static readonly IReadOnlyCollection<object[]> _versions;
		public IEnumerator<object[]> GetEnumerator() => _versions.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}

	public class BadMetadataStrings : IEnumerable<object[]> {
		static BadMetadataStrings() {
			var metadatas = new[] { "+", "+meta-data", "+meta+data" };

			_versions = metadatas.Select(i => new object[] { i }).ToList();
		}

		public static int Length => _versions.Count;

		private static readonly IReadOnlyCollection<object[]> _versions;
		public IEnumerator<object[]> GetEnumerator() => _versions.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}

	public class BadVersions : IEnumerable<object[]> {
		static BadVersions() {
			var metadatas = new[] { "+", "+meta-data", "+meta+data" };

			var versions = new List<string>(BadPrereleaseStrings.Length * metadatas.Length);

			foreach (var prerelease in new BadPrereleaseStrings().Select(i => i.First() as string)) {
				foreach (var metadata in metadatas) {
					versions.Add($"1.2.3{prerelease}{metadata}");
				}
			}

			var specificBadStrings = new[] {
				null,
				string.Empty,
				"one",
				"1",
				"1.2",
				"1.2.",
				"1.2.3.",
				"1.2.3pre-meta",
				"1.2.3+meta-pre"
			};

			_versions = versions.Concat(specificBadStrings).Select(v => new object[] { new SimpleVersion { GeneratingString = v } }).ToList();
		}

		private static readonly IReadOnlyCollection<object[]> _versions;
		public IEnumerator<object[]> GetEnumerator() => _versions.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}

	public sealed class SimpleVersion {
		public int Major { get; set; }
		public int Minor { get; set; }
		public int Patch { get; set; }
		public string PreRelease { get; set; }
		public string BuildMetadata { get; set; }

		public string GeneratingString { get; set; }
	}
}
